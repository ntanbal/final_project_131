import matplotlib.pyplot as plot 
import pandas as pd, numpy as np
import csv
import collections
from pandas.plotting import table

def create_file():
    full_dates =  []
    class_A = []
    class_B = []
    class_C = []
    seasons = []
    dates = {"Jan": 0,"Feb":0, 'March':0, 'April':0, 'May':0,'June':0,'July':0, 'Aug':0, 'Sept':0, 'Oct': 0, 'Nov': 0, 'Dec':0}
    with open('bfro_reports_geocoded.csv', "r", encoding = "utf-8") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            date = row[8]
            season = row[4]
            seasons.append(season)
            classification = row[10]
            
            if classification == 'Class A':
                if len(date) != 0:
                    class_A.append(date)
            elif classification == 'Class B':
                if len(date) != 0:
                    class_B.append(date)
            else:
                if len(date) != 0 and date != 'date':
                    class_C.append(date)
           
            if len(date) == 0:
                pass
            else:
                full_dates.append(date)
                month = date[5:7]

                if month == '01':
                    dates['Jan'] += 1
                elif month == '02':
                    dates['Feb'] += 1
                elif month == '03':
                    dates['March'] += 1
                elif month == '04':
                    dates['April'] += 1
                elif month == '05':
                    dates['May'] += 1
                elif month == '06':
                    dates['June'] += 1
                elif month == '07':
                    dates['July'] += 1
                elif month == '08':
                    dates['Aug'] += 1
                elif month == '09':
                    dates['Sept'] += 1
                elif month == '10':
                    dates['Oct'] += 1
                elif month == '11':
                    dates['Nov'] += 1
                else:
                    dates['Dec'] += 1
    return dates, full_dates, class_A, class_B, seasons


def bar_graph(dates):
    plot.rcParams["figure.figsize"] = (11, 6)
    df = pd.DataFrame(dates.items(), columns = ['Month', 'Appearance'])
    df.plot.bar(x = 'Month', y = 'Appearance', rot = 10,color = ['darkslategrey', 'teal', 'cadetblue'])
    plot.xlabel("Months", fontsize= 15)
    plot.ylabel("Appearances", fontsize = 15)
    plot.title("Big Foot Appearances from 1869 - 2018", fontsize = 20)
    
    plot.show(block = True)



def main():
    dates, full_dates, class_A, class_B, seasons = create_file()
    bar_graph(dates)

main()