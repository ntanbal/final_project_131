import matplotlib.pyplot as plot 
import pandas as pd, numpy as np
import csv
import collections
from pandas.plotting import table

def create_file():
    full_dates =  []
    class_A = []
    class_B = []
    class_C = []
    seasons = []
    dates = {"Jan": 0,"Feb":0, 'March':0, 'April':0, 'May':0,'June':0,'July':0, 'Aug':0, 'Sept':0, 'Oct': 0, 'Nov': 0, 'Dec':0}
    with open('bfro_reports_geocoded.csv', "r", encoding = "utf-8") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            date = row[8]
            season = row[4]
            seasons.append(season)
            classification = row[10]
            
            if classification == 'Class A':
                if len(date) != 0:
                    class_A.append(date)
            elif classification == 'Class B':
                if len(date) != 0:
                    class_B.append(date)
            else:
                if len(date) != 0 and date != 'date':
                    class_C.append(date)
           
            if len(date) == 0:
                pass
            else:
                full_dates.append(date)
                month = date[5:7]

                if month == '01':
                    dates['Jan'] += 1
                elif month == '02':
                    dates['Feb'] += 1
                elif month == '03':
                    dates['March'] += 1
                elif month == '04':
                    dates['April'] += 1
                elif month == '05':
                    dates['May'] += 1
                elif month == '06':
                    dates['June'] += 1
                elif month == '07':
                    dates['July'] += 1
                elif month == '08':
                    dates['Aug'] += 1
                elif month == '09':
                    dates['Sept'] += 1
                elif month == '10':
                    dates['Oct'] += 1
                elif month == '11':
                    dates['Nov'] += 1
                else:
                    dates['Dec'] += 1
    return dates, full_dates, class_A, class_B, seasons

def pie_chart(seasons):
    pie_season = {}
    for season in seasons:
        if season == 'season':
            pass
        elif season not in pie_season.keys():
            pie_season[season] = 1
        else:
            pie_season[season] += 1
        
    df = pd.DataFrame(pie_season.items(), columns = ['Season','Value'])
    
    labels = 'Fall', 'Winter','Spring', 'Summer', 'Unknown'
    colors = ['#ff9999','#66b3ff','#99ff99','#ffcc99' ,'#beeeef']
    explode = (0.05,0.05,0.05,0.05, 0.05)
    fig1, ax1 = plot.subplots()
    ax1.pie(df['Value'],labels = labels, autopct = '%1.1f%%', colors = colors, explode = explode, startangle=90, pctdistance=0.85)
    plot.rcParams['font.size'] = 5000
    centre_circle = plot.Circle((0,0),0.70,fc='white')
    fig = plot.gcf()
    fig.gca().add_artist(centre_circle)
    plot.tight_layout()
    plot.title("Which Season is Bigfoot Commonly Seen", fontsize = 20, pad = 25)
    plot.axis('equal')
    plot.show()

def main():
    dates, full_dates, class_A, class_B, seasons = create_file()
    pie_chart(seasons)

main()
