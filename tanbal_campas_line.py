import matplotlib.pyplot as plot 
import pandas as pd, numpy as np
import csv
import collections
from pandas.plotting import table


def create_file():
    full_dates =  []
    class_A = []
    class_B = []
    class_C = []
    seasons = []
    dates = {"Jan": 0,"Feb":0, 'March':0, 'April':0, 'May':0,'June':0,'July':0, 'Aug':0, 'Sept':0, 'Oct': 0, 'Nov': 0, 'Dec':0}
    with open('bfro_reports_geocoded.csv', "r", encoding = "utf-8") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            date = row[8]
            season = row[4]
            seasons.append(season)
            classification = row[10]
            
            if classification == 'Class A':
                if len(date) != 0:
                    class_A.append(date)
            elif classification == 'Class B':
                if len(date) != 0:
                    class_B.append(date)
            else:
                if len(date) != 0 and date != 'date':
                    class_C.append(date)
           
            if len(date) == 0:
                pass
            else:
                full_dates.append(date)

    return class_A, class_B

def design_line_chart(class_A, class_B):
    year_A = {}
    year_B = {}

    for num in class_A:
        year = num[:4]
        if year not in year_A.keys():
            year_A[year] = 1
        else:
            year_A[year] += 1
    for num in class_B:
        year = num[:4]
        if year not in year_B.keys():
            year_B[year] = 1
        else:
            year_B[year] += 1
   
    return year_A, year_B

def line_chart(year_A, year_B):
    odA = collections.OrderedDict(sorted(year_A.items()))
    odB = collections.OrderedDict(sorted(year_B.items()))

    dfA = pd.DataFrame(odA.items(), columns = ['Year', 'Appearances'])
    dfB = pd.DataFrame(odB.items(), columns = ['Year', 'Appearances'])

    ax = dfA.plot(x='Year', y='Appearances', label="Class A Sightings", color="steelblue", lw=3)
    dfB.plot(ax=ax, x='Year', y='Appearances', label="Class B Sightings", color="deepskyblue", lw=3)

    plot.title("Big Foot Appearances from 1925 - 2019", fontsize = 20)
    plot.ylabel("Appearances", fontsize = 20)
    plot.xlabel("Years", fontsize = 20)


    plot.show()

def main():
    class_A, class_B = create_file()
    year_A, year_B = design_line_chart(class_A, class_B)
    line_chart(year_A, year_B)    

main()