import matplotlib.pyplot as plot 
import pandas as pd, numpy as np
import csv
import collections
from pandas.plotting import table


def create_file():
    full_dates =  []
    class_A = []
    class_B = []
    class_C = []
    seasons = []
    dates = {"Jan": 0,"Feb":0, 'March':0, 'April':0, 'May':0,'June':0,'July':0, 'Aug':0, 'Sept':0, 'Oct': 0, 'Nov': 0, 'Dec':0}
    with open('bfro_reports_geocoded.csv', "r", encoding = "utf-8") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            date = row[8]
            season = row[4]
            seasons.append(season)
            classification = row[10]
            
            if classification == 'Class A':
                if len(date) != 0:
                    class_A.append(date)
            elif classification == 'Class B':
                if len(date) != 0:
                    class_B.append(date)
            else:
                if len(date) != 0 and date != 'date':
                    class_C.append(date)
           
            if len(date) == 0:
                pass
            else:
                full_dates.append(date)

    return full_dates

def design_scatterplot(full_dates):
    years_of_bf = {}
    for date in full_dates:
        year = date[0:4]
        if year == 'date':
            pass 

        elif year in years_of_bf.keys():
            years_of_bf[year] += 1
        else:
            years_of_bf[year] = 1
    return years_of_bf

def scatter_plot(years_of_bf):
    years_bf = collections.OrderedDict(sorted(years_of_bf.items()))
    df = pd.DataFrame(years_bf.items(), columns = ['Year','Appearances'])
    xdata = [int(v) for v in df['Year'].values]
    ydata = [int(v) for v in df['Appearances'].values]
    plot.plot(xdata,ydata,'co')

    
    plot.xlim(1920,2020)
   
    plot.grid()
    plot.xlabel("Year")
    plot.ylabel("Appearances")
    
    x = np.array(xdata)
    y = np.array(ydata)
    m, b = np.polyfit(x, y, 1)
    plot.plot(x, m*x + b, lw=3, color="darkslategrey")
    plot.ylim(bottom=0)

    plot.title("Big Foot Appearances from 1920 - 2018", fontsize = 15)
    plot.ylabel("Appearances", fontsize = 15)
    plot.xlabel("Years", fontsize = 15)

    plot.show(block=True)

def main():
    full_dates= create_file()
    years_of_bf = design_scatterplot(full_dates)
    scatter_plot(years_of_bf)
    
main()